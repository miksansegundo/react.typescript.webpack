
import * as React from 'react'
import * as ReactDOMServer from 'react-dom/server'

import Home from './containers/Home'
import Other from './containers/Other'

import { store } from './libs/store'
import { providePage } from './libs/dom.utils'
import { connectProps } from './libs/connector'

module.exports = function (pageName) {
  let Page
  switch (pageName) {
    case 'Home':
      Page = Home
    break
    case 'Other':
      Page = Other
  }
  const PageConnected = connectProps(Page)
  return ReactDOMServer.renderToString(providePage(<PageConnected />, store))
}