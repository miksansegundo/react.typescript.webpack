import * as React from 'react'
require('./styles.css')
const classNames = require('classnames')

export interface IMenuProps {
  actions:{
    navigate: (event) => void
  },
  menuActive: string
  menuItems: {
    name: string,
    url: string
  }[]
}

export function Menu (props: IMenuProps) {
  const { navigate } = props.actions
  const { menuActive, menuItems } = props

  return (
    <nav role='menu' >
      <ul className="nav navbar-nav">
        {menuItems.map((item) => (
          <li key={item.url} className={classNames({'active': menuActive === item.url})}>
            <a href={item.url} data-url={item.url}
               onClick={navigate}>{item.name}</a>
          </li>
        ))}
      </ul>
    </nav>
  )
}