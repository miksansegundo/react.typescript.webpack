import * as React from 'react'
import { Menu } from '../Menu/Menu'
import User from '../User/User'
require('./styles.css')
import { IMenuProps } from '../Menu/Menu'
import { IUserProps } from '../User/User'

interface IHeaderProps extends IMenuProps, IUserProps {
  brand: string,
  actions: any
}

export function Header (props: IHeaderProps) {
  return (
    <div>
      <nav className="navbar navbar-default">
        <div className="container-fluid">
          <div className="navbar-header">
            <button type="button" className="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar" />
              <span className="icon-bar" />
              <span className="icon-bar" />
            </button>
            <a className="navbar-brand" href="/">{props.brand}</a>
          </div>
          <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <Menu {...props} />
            <User {...props} />
          </div>
        </div>
      </nav>
    </div>
  )
}