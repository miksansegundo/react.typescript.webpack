import actionCreatorFactory from 'redux-typescript-actions'
const actionCreator = actionCreatorFactory()
import { loading } from './global.actions'
import * as api from '../apis/http.api'

export const setDataAction = actionCreator<{type: string, data: any}>('SET_DATA')
export const setSearchAction = actionCreator<string>('SET_SEARCH')
export const getUserAction = actionCreator<{token: string}>('FETCH_USER')
export const loginAction = actionCreator<{user: string, pwd: string}>('LOGIN_USER')
export const logoutAction = actionCreator<{token: string}>('LOGOUT_USER')
export const setUserAction = actionCreator<{}>('SET_USER')

export function getDataAction (action) {
  return (dispatch) => {
    dispatch(loading(true))
    api.getData(action.type)
      .then((data) => {
        return dispatch(setDataAction({type: action.type, data}))
      })
      .then(() => {
        dispatch(loading(false))
      })
  }
}