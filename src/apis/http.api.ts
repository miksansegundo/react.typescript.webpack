import axios from 'axios'

export function getData (type) {
  return axios.get(`https://jsonplaceholder.typicode.com/${type}`)
    .then(function ajax (response) {
      return response.data
    })
    .catch(function errorAjax (data) {
      console.error(data)
    })
}