
import * as React from 'react'
import { Header } from '../components/Header/Header'
import List from '../components/List/List'
import { Footer } from '../components/Footer/Footer'
import { Loading } from '../components/Loading/Loading'
import { connect } from 'react-redux'

interface IProps {}

const Home = React.createClass<IProps, any>({
  displayName: 'Home',
  componentDidMount() {
    this.props.actions.getDataAction({type: 'photos'})
  },
  render() {
    return (
      <div>
        <Header {...this.props} />
        <div className="page-header">
          <h1>Home</h1>
        </div>
        <Loading {...this.props} >
          <List {...this.props} />
        </Loading>
        <Footer />
      </div>
    )
  }
})

export default connect()(Home)