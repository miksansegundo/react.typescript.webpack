import { combineEpics } from 'redux-observable'
import { fetchUserEpic } from './epics'

export const rootEpic = combineEpics(fetchUserEpic)
