
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/dom/ajax'
import 'rxjs/add/observable/of'
import 'rxjs/add/observable/merge'
import 'rxjs/add/operator/mergeMap'
import 'rxjs/add/operator/switchMap'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'

import {setUserAction, loginAction} from '../actions/data.actions'
import {authenticating, setLoginErrorAction} from '../actions/global.actions'

interface IPayload {
 name: string
}

export const fetchUserEpic = (action$) =>
  action$.ofType(loginAction.type)
    .switchMap(action => {
      const {user, pwd} = action.payload
      return Observable.merge(
        Observable.of(authenticating(true)),
        Observable.ajax
          .getJSON(`https://api.github.com/users/${user}?pwd=${pwd}`)
          .switchMap((payload: IPayload) => {
            if (!payload.name) {
              return Observable.of(setLoginErrorAction({msg: 'User or password incorrect'}), authenticating(false))
            }
            return Observable.of(setUserAction(payload), authenticating(false))
          })
          .catch(() => Observable.of(setLoginErrorAction({msg: 'User is not registered'}), authenticating(false)))
        )
    })