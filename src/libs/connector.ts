import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as routerActions from '../actions/router.actions'
import * as globalActions from '../actions/global.actions'
import * as dataActions from '../actions/data.actions'

export function connectProps (Page) {
  const mapStateToProps = (state) => {
    return Object.assign({},
      state.router,
      state.global,
      state.data
    )
  }
  function mapDispatchToProps(dispatch) {
    return { actions: bindActionCreators(Object.assign({},
      routerActions,
      globalActions,
      dataActions
    ), dispatch)}
  }
  return connect(mapStateToProps, mapDispatchToProps)(Page)
}
