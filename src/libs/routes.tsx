import * as React from 'react'
import { renderPage, providePage } from './dom.utils'
import { connectProps } from './connector'
import { store } from './store'

// Pages Components
import Other from '../containers/Other'
import Home from '../containers/Home'

export default {
  '/': Home,
  '/other.html': Other
}

if (module.hot) {
  module.hot.accept('../containers/Home.tsx', () => {
    const Page = require('../containers/Home.tsx').default
    const PageConnected = connectProps(Page)
    renderPage(providePage(<PageConnected />, store))
  })

  module.hot.accept('../containers/Other.tsx', () => {
    const Page = require('../containers/Other.tsx').default
    const PageConnected = connectProps(Page)
    renderPage(providePage(<PageConnected />, store))
  })
}