import { Action as ReduxAction } from 'redux'
import { isType } from 'redux-typescript-actions'
import { loading, authenticating, setLoginErrorAction } from '../actions/global.actions'

const DEFAULT_STATE = {
  brand: 'Wedjago.com',
  loading: true,
  authenticating: false,
  loginError: false,
  loginErrorMsg: ''
}

function setLoading(state, action) {
  const newState = {}
  Object.assign(newState, state, {loading: action.payload})
  return newState
}

function setAuthenticating(state, action) {
  const newState = {
    ...state,
    authenticating: action.payload
  }
  if (action.payload) {
    newState.loginError = false
    newState.loginErrorMsg = ''
  }
  return newState
}

function setLoginError (state, action) {
  const newState = {
    ...state,
    loginError: true,
    loginErrorMsg: action.payload.msg
  }
  return newState
}

export function global (state = DEFAULT_STATE, action:ReduxAction) {
  if (isType(action, loading)) {
    return setLoading(state, action)
  }
  if (isType(action, authenticating)) {
    return setAuthenticating(state, action)
  }
  if (isType(action, setLoginErrorAction)) {
    return setLoginError(state, action)
  }
  return state
}
