import { Action as ReduxAction } from 'redux'
import { isType } from 'redux-typescript-actions'
import { setDataAction, setSearchAction } from '../actions/data.actions'
import { createSelector } from 'reselect'

const DEFAULT_STATE = {
  list: [],
  listFilter: {
    search: '',
    maxResults: 27
  }
}

const memoized = {
  lastResults: []
}

function setData (state, action) {
  const newState = {}
  Object.assign(newState, state, { list: action.payload.data })
  return newState
}

function setSearch (state, action) {
  return {
    ...state,
    listFilter: {
      ...state.listFilter,
      search: action.payload
    }
  }
}

export function data (state = DEFAULT_STATE, action:ReduxAction) {
  if (isType(action, setDataAction)) {
    return setData(state, action)
  }
  if (isType(action, setSearchAction)) {
    return setSearch(state, action)
  }
  return state
}

const getList = (state) => state.data.list
const getListFilter = (state) => state.data.listFilter
const getMaxResults = (state) => state.data.listFilter.maxResults
export const getSearch = (state) => state.data.listFilter.search
const getInitial = createSelector(
  getList, getMaxResults,
  (list, maxResults) => {
    return list.slice(0, maxResults)
  }
)

export const getListFiltered = createSelector(
  getListFilter, getList, getInitial,
  (listFilter, list, initialList) => {
    const search = listFilter.search.toUpperCase()

    if (!search || search.length === 0) {
      return initialList
    }
    if (search.length < 3) {
      return memoized.lastResults.length && memoized.lastResults || initialList
    }
    return memoized.lastResults = list
      .filter((item) => {
        return (item.title.toUpperCase().indexOf(search) > 0)
      })
      .slice(0, listFilter.maxResults)
  }
)